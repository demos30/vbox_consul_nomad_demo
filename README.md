
Environment prep:



```
$cat /etc/lsb-release 
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.10
DISTRIB_CODENAME=groovy
DISTRIB_DESCRIPTION="Ubuntu 20.10"
```

Start by installing virtualbox, vagrant, ansible and vagrant plugin for virtualbox:

```
$sudo apt install virtualbox
```
or for a less prepared machine:

```
sudo apt install virtualbox vagrant ansible
```

and

```
$vagrant plugin install virtualbox
```




